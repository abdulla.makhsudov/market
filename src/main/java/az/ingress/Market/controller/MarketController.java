package az.ingress.Market.controller;

import az.ingress.Market.dto.MarketRequestDto;
import az.ingress.Market.dto.MarketResponseDto;
import az.ingress.Market.model.Market;
import az.ingress.Market.service.MarketService;
import jakarta.validation.Valid;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class MarketController {

    private final MarketService marketService;

    @PostMapping
    public Long createMarket(@RequestBody @Valid MarketRequestDto request) {
        return marketService.createMarket(request);
    }

    @GetMapping
    public Page<Market> getMarket(@RequestParam("pageSize") int pageSize,
                                  @RequestParam(value = "pageNumber", required = false) int pageNumber,
                                  @RequestParam(defaultValue = "id,desc") String[] sort) {
        return marketService.getMarket(pageSize, pageNumber, sort);
    }

    @GetMapping("/cache")
    public Market getMarket2(@RequestParam String name) {
        return marketService.getMarket2(name);
    }

}
