package az.ingress.Market.controller;

import az.ingress.Market.model.Student;
import az.ingress.Market.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping
    public Student getStudent() {
        return studentService.getStudent();
    }

    @PutMapping
    public void updateStudent(@RequestBody Student student) {
        studentService.save(student);
    }
}
