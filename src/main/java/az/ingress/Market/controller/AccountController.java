package az.ingress.Market.controller;

import az.ingress.Market.dto.RequestAddressDto;
import az.ingress.Market.dto.SearchCriteria;
import az.ingress.Market.model.Account;
import az.ingress.Market.model.Address;
import az.ingress.Market.service.AccountService;
import az.ingress.Market.service.AddressService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @PostMapping
    public long saveAccount(@RequestBody Account account) {
        return accountService.saveAccount(account);
    }

    @GetMapping
    public Account getAccount(@RequestParam long id) {
        return accountService.getAccount(id);
    }

    @PutMapping("/1")
    public long updateAccount(@RequestBody Account account) {
        return accountService.updateAccount(account);
    }

    @PutMapping("/2")
    public long updateAccount2(@RequestBody Account account) {
        return accountService.updateAccount2(account);
    }

}
