package az.ingress.Market.controller;

import az.ingress.Market.dto.RequestAddressDto;
import az.ingress.Market.dto.SearchCriteria;
import az.ingress.Market.dto.SearchCriteriaCustom;
import az.ingress.Market.model.Address;
import az.ingress.Market.service.AddressService;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/address")
@RequiredArgsConstructor
public class AddressController {

    private final AddressService addressServiceImplB;

    @PostMapping
    public long saveAddress(@RequestBody RequestAddressDto request) {
        return addressServiceImplB.saveAddress(request);
    }

    @GetMapping("")
    public List<Address> getAddresses(@RequestParam(required = false) String name,
                                      @RequestParam(required = false) Long id,
                                      @RequestParam(required = false) String street,
                                      @RequestParam(required = false) String apartNo) {
        return addressServiceImplB.findSpecWithJpgl(name, id, street, apartNo);
    }

    @GetMapping("/spec")
    public List<Address> getAllAddressWithSpec(@RequestBody List<SearchCriteria> searchCriteria) {
        return addressServiceImplB.getAllAddressWithSpec(searchCriteria);
    }
}
