package az.ingress.Market.service.impl;

import az.ingress.Market.model.Student;
import az.ingress.Market.repository.StudentRepository;
import az.ingress.Market.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public Student getStudent() {
        return studentRepository.findByName("Tural").get();
    }

    @Override
    public void save(Student student) {
        studentRepository.save(student);
    }
}
