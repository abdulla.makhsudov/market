package az.ingress.Market.service.impl;

import static org.springframework.data.jpa.domain.Specification.where;

import az.ingress.Market.dto.AddressSpecification;
import az.ingress.Market.dto.RequestAddressDto;
import az.ingress.Market.dto.SearchCriteria;
import az.ingress.Market.model.Address;
import az.ingress.Market.repository.AddressRepository;
import az.ingress.Market.service.AddressService;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AddressServiceImplB implements AddressService {

    private final AddressRepository addressRepository;
    private final ModelMapper modelMapper;
    @Override
    public long saveAddress(RequestAddressDto request) {
        Address address = modelMapper.map(request, Address.class);
        return addressRepository.save(address).getId();
    }

    @Override
    public Address findById(long id) {
        Optional<Address> addressOptional = addressRepository.findById(id);
        addressOptional.orElseThrow(() -> new RuntimeException("Test"));
        return addressOptional.get();
    }

    @Override
    public List<Address> findSpec(String name, Long id, String street, String apartNo) {
        return addressRepository.findAll(where(withParams(name, id, street).or(byApartNo(apartNo))));
    }

    @Override
    public List<Address> findSpecWithJpgl(String name, Long id, String street, String apartNo) {
        return addressRepository.getAllDataWithParams(name, id, street, apartNo);
    }

    @Override
    public List<Address> getAllAddressWithSpec(List<SearchCriteria> searchCriteria) {
        AddressSpecification addressSpecification = new AddressSpecification();
//        addressSpecification.add(searchCriteria);
        searchCriteria.forEach(criteria -> addressSpecification.add(criteria));
        return addressRepository.findAll(addressSpecification);
    }


//    public List<Address> findSpec(String name) {
//        return addressRepository.findAll(byName(name));
//    }

    private Specification<Address> byApartNo(String apartNo) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("apartNo"), apartNo);
    }

    public Specification<Address> withParams(String name, Long id, String street) {

        Specification<Address> specification = null;

        if (name != null) {
            specification = (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("addressName"), name);
        }

        if (id != null) {
            specification = (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("id"), id);
        }

        if (street != null) {
            specification = (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("street"), street);
        }

        return specification;
    }
}
