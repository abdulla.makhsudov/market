package az.ingress.Market.service.impl;

import az.ingress.Market.config.AppConfiguration;
import az.ingress.Market.dto.MarketRequestDto;
import az.ingress.Market.dto.MarketResponseDto;
import az.ingress.Market.model.Branch;
import az.ingress.Market.model.Market;
import az.ingress.Market.repository.MarketRepository;
import az.ingress.Market.service.MarketService;
import jakarta.annotation.Resource;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class MarketServiceImpl implements MarketService {

    private final MarketRepository marketRepository;
    private final AppConfiguration appConfiguration;

    @Override
    public Long createMarket(MarketRequestDto request) {
        Market market = appConfiguration.getMapper().map(request, Market.class);
        market.getBranch().add(Branch.builder().build());
        return marketRepository.save(market).getId();
    }

    @Override
    public Page<Market> getMarket(int pageSize, int pageNumber, String[] sort) {
        PageRequest pageRequest = PageRequest.of(pageSize, pageNumber, Sort.by(sort[0], sort[1]).descending());
        Page<Market> marketList = marketRepository.findAll(pageRequest);
//        List<MarketResponseDto> marketResponseDtoList = new ArrayList<>();
//        marketList.forEach(entity -> {
//            MarketResponseDto marketResponseDto =
//                    appConfiguration.getMapper().map(entity, MarketResponseDto.class);
//            marketResponseDtoList.add(marketResponseDto);
//        });
        return marketList;
    }

    @Override
    public Market getMarketWithId(Long id) {
        return marketRepository.findById(id).orElseThrow(() -> new RuntimeException("Market not found with id" + id));
    }

    @Override
//    @Cacheable(value = "cache", cacheNames = "")
//    @CachePut("cache")
    public Market getMarket2(String name) {
        Market araz = marketRepository.findByMarketName("Araz");
        araz.setMarketName(name);
        return marketRepository.save(araz);
    }

    public void setName2(String name2) {
       setName(name2);
    }

    @Transactional
    public Market setName(String name) {
        Market market = marketRepository.findById(1L).get();
        market.setMarketName(name);
//        marketRepository.save(market);
//        market.setMarketName(name + name);
        return market;
    }


    public Market getMarketWithName() {
        return marketRepository.findByMarketName("Araz");
    }

}
