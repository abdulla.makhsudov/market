package az.ingress.Market.service.impl;

import az.ingress.Market.model.Account;
import az.ingress.Market.repository.AccountRepository;
import az.ingress.Market.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    @Override
    public long saveAccount(Account account) {
        return accountRepository.save(account).getId();
    }

    @Override
    @Transactional
    public Account getAccount(long id) {
        return accountRepository.findById(id).get();
    }

    @Override
    @SneakyThrows
    @Transactional
    public long updateAccount(Account account) {
        long threadId = Thread.currentThread().getId();
        log.info("Start transaction with thread id: {}", threadId);
        Account accountFromDb = accountRepository.findById(account.getId()).get();
        log.info("Thread id: {}, account: {}", threadId, accountFromDb);
        accountFromDb.setBalance(accountFromDb.getBalance() + account.getBalance() );
        Account accountFromDb2 = accountRepository.findById(2L).get();
        accountFromDb2.setBalance(accountFromDb2.getBalance() - account.getBalance());
        Thread.sleep(10000);
        log.info("Thread id: {}, modifiedAccount: {}", threadId, accountFromDb);
        Account savedAccount = accountRepository.save(accountFromDb);
        accountRepository.save(accountFromDb2);
        log.info("Thread id: {}, savedAccount: {}", threadId, savedAccount);
        return savedAccount.getId();
    }

    @Override
    @Transactional
    public long updateAccount2(Account account) {
        long threadId = Thread.currentThread().getId();
        log.info("Start transaction with thread id: {}", threadId);
        Account accountFromDb = accountRepository.findById(account.getId()).get();
        log.info("Thread id: {}, account: {}", threadId, accountFromDb);
        accountFromDb.setBalance(accountFromDb.getBalance() + account.getBalance() );
        Account accountFromDb2 = accountRepository.findById(2L).get();
        accountFromDb2.setBalance(accountFromDb2.getBalance() - account.getBalance());
        log.info("Thread id: {}, modifiedAccount: {}", threadId, accountFromDb);
        Account savedAccount = accountRepository.save(accountFromDb);
        accountRepository.save(accountFromDb2);
        log.info("Thread id: {}, savedAccount: {}", threadId, savedAccount);
        return savedAccount.getId();
    }
}
