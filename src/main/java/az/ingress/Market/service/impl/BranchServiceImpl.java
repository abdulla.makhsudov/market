package az.ingress.Market.service.impl;

import az.ingress.Market.dto.BranchQueryDto;
import az.ingress.Market.dto.RequestBranchDto;
import az.ingress.Market.model.Address;
import az.ingress.Market.model.Branch;
import az.ingress.Market.model.Market;
import az.ingress.Market.repository.BranchRepository;
import az.ingress.Market.service.AddressService;
import az.ingress.Market.service.BranchService;
import az.ingress.Market.service.MarketService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class BranchServiceImpl implements BranchService {

    private final BranchRepository branchRepository;
    private final MarketService marketService;
    private final AddressService addressServiceImplB;
    private final ModelMapper modelMapper;

    @Value("${test}")
    private String port;

    @Override
    public long saveBranch(RequestBranchDto request) {
        Address address = addressServiceImplB.findById(request.getAddress_id());
        Market market = marketService.getMarketWithId(request.getMarket_id());
        Branch branch = Branch.builder()
                .branchName(request.getBranchName())
                .address(address)
                .market(market)
                .build();
        return branchRepository.save(branch).getId();
    }

    @Override
    public BranchQueryDto getBranch() {
//        Branch branch = branchRepository.findById(3L).get();
        BranchQueryDto branchQueryDto = branchRepository.getBranchWithCustomDtoQuery();
        log.info("Getting branch from DB : {}", branchQueryDto);
        return branchQueryDto;
    }

    @Override
    public List<Branch> getAllBranch() {
        return branchRepository.findAll();
    }
}
