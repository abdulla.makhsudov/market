package az.ingress.Market.service;

import az.ingress.Market.dto.MarketRequestDto;
import az.ingress.Market.dto.MarketResponseDto;
import az.ingress.Market.model.Market;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MarketService {
    Long createMarket(MarketRequestDto request);

    Page<Market> getMarket(int pageSize, int pageNumber, String[] sort);

    Market getMarketWithId(Long id);

    Market getMarket2(String name);
}
