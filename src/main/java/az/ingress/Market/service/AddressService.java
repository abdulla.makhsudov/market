package az.ingress.Market.service;

import az.ingress.Market.dto.RequestAddressDto;
import az.ingress.Market.dto.SearchCriteria;
import az.ingress.Market.dto.SearchCriteriaCustom;
import az.ingress.Market.model.Address;
import java.util.List;

public interface AddressService {
    long saveAddress(RequestAddressDto request);
    Address findById(long id);

    List<Address> findSpec(String name, Long id, String street, String apartNo);
    List<Address> findSpecWithJpgl(String name, Long id, String street, String apartNo);

    List<Address> getAllAddressWithSpec(List<SearchCriteria> searchCriteria);
}
