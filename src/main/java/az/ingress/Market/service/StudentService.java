package az.ingress.Market.service;

import az.ingress.Market.model.Student;

public interface StudentService {
    Student getStudent();

    void save(Student student);
}
