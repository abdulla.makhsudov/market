package az.ingress.Market.service;

import az.ingress.Market.dto.BranchQueryDto;
import az.ingress.Market.dto.RequestBranchDto;
import az.ingress.Market.model.Branch;
import java.util.List;

public interface BranchService {
    long saveBranch(RequestBranchDto request);

    BranchQueryDto getBranch();

    List<Branch> getAllBranch();
}
