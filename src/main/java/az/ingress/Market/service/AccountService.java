package az.ingress.Market.service;

import az.ingress.Market.model.Account;

public interface AccountService {

    long saveAccount(Account account);

    Account getAccount(long id);

    long updateAccount(Account account);

    long updateAccount2(Account account);
}
