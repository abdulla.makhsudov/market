package az.ingress.Market.exception;

import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    public Map<String, String> handleException(RuntimeException exception) {
        Map<String, String> error = new HashMap<>();
        error.put("exceptionMessage", exception.getLocalizedMessage());
        return error;
    }
}
