package az.ingress.Market.dto;

public class Fish implements IAnimal{
    @Override
    public boolean canFly() {
        throw new RuntimeException();
    }

    @Override
    public boolean canSwim() {
        return false;
    }

    @Override
    public boolean canRun() {
        return false;
    }
}
