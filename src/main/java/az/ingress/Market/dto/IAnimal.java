package az.ingress.Market.dto;

public interface IAnimal {
    boolean canFly();
    boolean canSwim();
    boolean canRun();
}
