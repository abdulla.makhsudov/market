package az.ingress.Market.dto;

public class Bird implements IAnimal{

    @Override
    public boolean canFly() {
        return true;
    }

    @Override
    public boolean canSwim() {
        throw new RuntimeException();
    }

    @Override
    public boolean canRun() {
        throw  new RuntimeException();
    }
}
