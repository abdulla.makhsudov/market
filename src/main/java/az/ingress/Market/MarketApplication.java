package az.ingress.Market;

import az.ingress.Market.model.Lesson;
import az.ingress.Market.model.Market;
import az.ingress.Market.model.Student;
import az.ingress.Market.repository.LessonRepository;
import az.ingress.Market.repository.MarketRepository;
import az.ingress.Market.repository.StudentRepository;
import az.ingress.Market.service.MarketService;
import az.ingress.Market.service.impl.MarketServiceImpl;
import jakarta.annotation.Resource;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
@EnableCaching
public class MarketApplication implements CommandLineRunner {

	private final StudentRepository studentRepository;
	private final LessonRepository lessonRepository;
	private final Environment environment;
	private final MarketRepository marketRepository;
	@PersistenceContext
	private final EntityManager entityManager;
	@Resource
	private final MarketServiceImpl marketService;


	//NamedQuery
	//fetch type, Lazy Init Exception
	//EntityGraph
	//Specification

	//Pageable
	//Isolation level
	//PersistenceContext
	//L1 cache

	//Transaction pitfalls
	//Reference types
	//Transaction propagition


	public static void main(String[] args) {
		SpringApplication.run(MarketApplication.class, args);
	}

	@Override
	@Transactional
	public void run(String... args) throws Exception {
		Market market = marketService.getMarketWithName();
		System.out.println(market);
	}
}
