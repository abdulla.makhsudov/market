package az.ingress.Market.repository;

import az.ingress.Market.dto.BranchQueryDto;
import az.ingress.Market.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BranchRepository extends JpaRepository<Branch, Long> {

    @Query(value = "select new az.ingress.Market.dto.BranchQueryDto(b.id, b.branchName, a.addressName, m.marketName) from Branch b join b.market m join b.address a ")
    BranchQueryDto getBranchWithCustomDtoQuery();

    @Query(value = "select b from Branch b join fetch b.market m join fetch b.address a")
    Branch getBranchJpql();



}
