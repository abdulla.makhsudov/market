package az.ingress.Market.repository;

import az.ingress.Market.model.Address;
import java.util.List;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AddressRepository extends JpaRepository<Address, Long>, JpaSpecificationExecutor<Address> {
    @Query("select a from Address a where (" +
            "(:name is null or a.addressName=:name)" +
            "and (:id is null or a.id=:id)" +
            "and (:street is null or a.street=:street)" +
            "or (:apartNo is null or a.apartNo=:apartNo)" +
            ")"
    )
    List<Address> getAllDataWithParams(@Param("name") String name, @Param("id") Long id, @Param("street") String street,
                                       @Param("apartNo") String apartNo);

}
