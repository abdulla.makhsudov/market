package az.ingress.Market.repository;

import az.ingress.Market.model.Student;
import java.util.Optional;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
//    @EntityGraph(attributePaths = {"courses"}, type = EntityGraph.EntityGraphType.FETCH)
//    Student findById(String name);

    Optional<Student> findByName(String name);
}
